#ifndef __HW_IPU_H__
#define __HW_IPU_H__

int ipu_get_sdc_channel_params(uint32_t *buf, size_t cnt);

#endif // __HW_IPU_H__
